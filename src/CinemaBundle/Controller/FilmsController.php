<?php

namespace CinemaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CinemaBundle\Entity\Films;
use CinemaBundle\Form\FilmsType;

/**
 * Films controller.
 *
 * @Route("/")
 */
class FilmsController extends Controller
{
    /**
     * Lists all Films entities.
     *
     * @Route("/", name="_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $films = $em->getRepository('CinemaBundle:Films')->findAll();

        return $this->render('films/index.html.twig', array(
            'films' => $films,
        ));
    }

    /**
     * Creates a new Films entity.
     *
     * @Route("/new", name="_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $film = new Films();
        $form = $this->createForm('CinemaBundle\Form\FilmsType', $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($film);
            $em->flush();

            return $this->redirectToRoute('_show', array('id' => $film->getId()));
        }

        return $this->render('films/new.html.twig', array(
            'film' => $film,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Films entity.
     *
     * @Route("/{id}", name="_show")
     * @Method("GET")
     */
    public function showAction(Films $film)
    {
        $deleteForm = $this->createDeleteForm($film);

        return $this->render('films/show.html.twig', array(
            'film' => $film,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Films entity.
     *
     * @Route("/{id}/edit", name="_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Films $film)
    {
        $deleteForm = $this->createDeleteForm($film);
        $editForm = $this->createForm('CinemaBundle\Form\FilmsType', $film);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($film);
            $em->flush();

            return $this->redirectToRoute('_edit', array('id' => $film->getId()));
        }

        return $this->render('films/edit.html.twig', array(
            'film' => $film,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Films entity.
     *
     * @Route("/{id}", name="_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Films $film)
    {
        $form = $this->createDeleteForm($film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($film);
            $em->flush();
        }

        return $this->redirectToRoute('_index');
    }

    /**
     * Creates a form to delete a Films entity.
     *
     * @param Films $film The Films entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Films $film)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('_delete', array('id' => $film->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
