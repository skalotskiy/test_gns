<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/home", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
        ]);
    }


    /**
     * Creates a new Films entity.
     *
     * @Route("/test", name="default_test")
     */
    public function testAction()
    {
        $arr = array(1 => "", 2 => "");
        $arr= array_map(array($this, "randNumber"), $arr);




        $test_code = '
        $arr = array(1 => "", 2 => "");
        $arr= array_map(array($this, "randNumber"), $arr);

        private function randNumber($a){
            return  (int)$a+ rand(1, 9999);
        }       ';







        return $this->render('default/test.html.twig', [
            'test_code' => htmlentities($test_code),
        ]);
    }


    private function randNumber($a)
    {
        return  (int)$a+ rand(1, 9999);
    }


}
